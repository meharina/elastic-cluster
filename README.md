# Creating an Elasticsearch Cluster:
- Starting off
  - Network diagraming with Lucid

## Create a VPC
- step 1
- add subnet
- custom VPC already adds a Internt Gatway (igw)
- add subnet to route table

- step 2 (running on instances)
- 5 instances 
  - 2 hot-nodes, 2 warm-nodes, 1 cold-nodes

## Initial Installation
### Install Java
- sudo yum install java-11-openjdk-devel
- java -version
## Instal Elasticserach
### https://linuxize.com/post/how-to-install-elasticsearch-on-centos-8/
- sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
- sudo yum install nano
- sudo nano /etc/yum.repos.d/elasticsearch.repo
```bash
[elasticsearch-7.x]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```
- sudo yum install elasticsearch
- sudo systemctl enable elasticsearch.service --now
- curl -X GET "localhost:9200/"
#### Should get this response
```json
{
  "name" : "centos8.localdomain",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "V_mfjn2PRJqX3PlZb_VD7w",
  "version" : {
    "number" : "7.6.0",
    "build_flavor" : "default",
    "build_type" : "rpm",
    "build_hash" : "7f634e9f44834fbc12724506cc1da681b0c3b1e3",
    "build_date" : "2020-02-06T00:09:00.449973Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```
- if errors check "sudo journalctl -u elasticsearch"

### work on ansible scripting (continous)
- check ansible inventory, playbooks, etc

- created a /elastic foler to store all artifacts (kesy,code, etc)
- sudo mkdir /elastic
- sudo chown centos:centos /elastic/
    - changed owner to centos instead of root

- deleted intial instance "warm-node-1"
- created instance "ansible-controller"

### side note: installed git
- installed git on the instance 
- installed git on my pc (follow git instructions)

- added the key to the ansible controller
  - put the /elastic folder
  - sudo chmod 600  elastic-cluster-key.pem 
- sudo yum install python3
- sudo pip install virtualenv  
  - downloaded from python/pip, not from yum
  - pip is pythons package installer
  - created virtualenv to seperate python packages (download to virtual enviroment)

### creating envornment
- sudu su -
- pip install virtualenv
   38  cd /usr
   39  cd local/share/
   40  ll
   41  mkdir virtualenv
   42  sudo mkdir virtualenv
   43  cd virtualenv/
   44  virtualenv /usr/local/share/virtualenv/ansible
   47  source /usr/local/share/virtualenv/ansible/bin/activate

### Need to use pip instead of pip3
#### install anisble with pip 
- url https://bootstrap.pypa.io/get-pip.py -o get-pip.py
- sudo python get-pip.py
- cd /elastic/
- python -m virtualenv ansible
  - "ansible" is the name of the enviro
- sudo su -
- pip install virtualenv
- source ansible/bin/activate
  - activated the virtual environment

### once in enviro
- https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-with-pip
- installing ansible
- python -m pip install ansible
- python -m pip install paramiko
- ansible (error because using verision python2)
- pip uninstall cryptography
- pip install cryptography==2.9.2  (only for ansibler vault)
- pip install boto3 (to use ansible with aws)

Creating VPC
- in virtiual enviro
- ansible-galaxy collection install amazon.aws
- pip install boto botocore

### Create a vault file
- Create a .vault file
  - added a randomly generatered key
- encrypted the access key and secret key
- ansible-vault encrypt_string --vault-password-file .vault 'access key' --name 'name for key'
  - paste into the group_vars all.yml file
  - repeat steps for the secret key
- add the password file '.vault' to the ansible.cfg  
  - vault_password_file = ./.vault
  #### check ansible.cfg documentation for more on ansible.cfg

next steps:

- using the route table facts (so we don't create another route table everytime)
- create the instances (hot, warm, cold)
- make sure to stop ansible controller (terminate everythinng else)

## Git Cmds to Transfer 
- git status
- git add .
- git commit -m 'update name for what you're saving'
- git config --global user.name "Nebiyu"
  - saves to the my local drive
- git config --global user.email meharina321@gmail.com
- git commit -m 'update name for what you're saving'
- git push origin HEAD

## Create instances
- add public facing instance in the public subnet 
- add private instances in th private subnet (5 nodes)
-  For geting inventory host ips
- '{{ hostvars["warm-node-1"].ansible_host }}'


# COME BACK AND FILL OUT THE NOTES
- added a loop and async poll to create instances in one task (check ansible)
  - correct subnets / other issues
- created key pairs and attached to the instances
- VPC peering between 2 regions 
  - adjusted route tables
  - used VPC peering
- updated the instances
- (skipped) need to run updates on private instances through the public node

## Installing Elasticsearch and working on configuration
- created ansible script 
  - check site (https://linuxize.com/post/how-to-install-elasticsearch-on-centos-7/)




